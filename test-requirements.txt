molecule>=3.0.2
yamllint>=1.20.0
jmespath>=0.9.4
docker
ansible-lint>=4.2.0
testinfra>=4.1.0
cryptography
