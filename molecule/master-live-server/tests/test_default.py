
import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/etc/ansible/facts.d",
    "/opt/coremedia/master-live-server",
    "/opt/coremedia/master-live-server/bin",
    "/var/log/coremedia/master-live-server"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/master-live-server.fact",
    "/opt/coremedia/master-live-server/master-live-server.jar",
    "/opt/coremedia/master-live-server/application.properties",
    "/opt/coremedia/master-live-server/bin/post-start-check.sh",
    "/opt/coremedia/master-live-server/jmxremote.access",
    "/opt/coremedia/master-live-server/jmxremote.password",
    "/opt/coremedia/master-live-server/jaas.conf",
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("master-live-server").exists
    assert host.group("coremedia").exists
    assert host.user("master-live-server").shell == "/sbin/nologin"
    assert host.user("master-live-server").home == "/opt/coremedia/master-live-server"


def test_property_file(File):
    property_file = "/opt/coremedia/master-live-server/application.properties"
    content = File(property_file).content
    assert "cap.server.search.enable(.*)=(.*)false" in content


def test_properties(host):
    property_file = "/opt/coremedia/master-live-server/application.properties"
    content = host.file(property_file).content_string

    print(content)

    assert re.compile("cap.server.search.enable.*false").search(content)
    assert re.compile("cap.server.license.*=.*mls.zip").search(content)
    assert re.compile("replicator.publicationIorUrl").search(content)


def test_service(host):
    service = host.service("master-live-server")
    assert service.is_enabled
    assert service.is_running


@pytest.mark.parametrize("ports", [
    '0.0.0.0:40299',
    '0.0.0.0:40280',
    '0.0.0.0:40281'
])
def test_open_port(host, ports):

    for i in host.socket.get_listening_sockets():
        print( i )

    application = host.socket("tcp://{}".format(ports))
    assert application.is_listening
