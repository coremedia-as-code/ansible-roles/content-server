import pytest
import os
import re
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/etc/ansible/facts.d",
    "/opt/coremedia/replication-live-server",
    "/opt/coremedia/replication-live-server/bin",
    "/var/log/coremedia/replication-live-server"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/replication-live-server.fact",
    "/opt/coremedia/replication-live-server/replication-live-server.jar",
    "/opt/coremedia/replication-live-server/application.properties",
    "/opt/coremedia/replication-live-server/bin/post-start-check.sh",
    "/opt/coremedia/replication-live-server/jmxremote.access",
    "/opt/coremedia/replication-live-server/jmxremote.password",
    "/opt/coremedia/replication-live-server/jaas.conf",
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("replication-live-server").exists
    assert host.group("coremedia").exists
    assert host.user("replication-live-server").shell == "/sbin/nologin"
    assert host.user("replication-live-server").home == "/opt/coremedia/replication-live-server"


def test_properties(host):
    property_file = "/opt/coremedia/replication-live-server/application.properties"
    content = host.file(property_file).content_string

    assert re.compile("cap.server.search.enable.*false").search(content)
    assert re.compile("cap.server.license.*=.*rls.zip").search(content)
    assert re.compile("replicator.publicationIorUrl").search(content)
    assert re.compile("sql.store.driver").search(content)
    assert re.compile("sql.store.password").search(content)
    assert re.compile("sql.store.url.*=.*jdbc.*").search(content)


def test_loggentry(host):
    property_file = "/var/log/coremedia/replication-live-server/replication-live-server.log"
    content = host.file(property_file).content_string

    assert re.compile(".*Replicator: creating and starting pipeline*").search(content)


def test_service(host):
    service = host.service("replication-live-server")
    assert service.is_enabled
    assert service.is_running


@pytest.mark.parametrize("ports", [
    '0.0.0.0:42099',
    '0.0.0.0:42080',
    '0.0.0.0:42081'
])
def test_open_port(host, ports):

    for i in host.socket.get_listening_sockets():
        print( i )

    application = host.socket("tcp://{}".format(ports))
    assert application.is_listening
