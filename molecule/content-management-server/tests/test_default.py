import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/content-management-server",
    "/opt/coremedia/content-management-server/bin",
    "/var/log/coremedia/content-management-server"
    "/var/cache/coremedia/content-management-server/blobstore-assets"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/content-management-server.fact",
    "/opt/coremedia/content-management-server/content-management-server.jar",
    "/opt/coremedia/content-management-server/application.properties",
    "/opt/coremedia/content-management-server/bin/post-start-check.sh",
    "/opt/coremedia/content-management-server/jmxremote.access",
    "/opt/coremedia/content-management-server/jmxremote.password",
    "/opt/coremedia/content-management-server/jaas.conf",
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("content-management-server").exists
    assert host.group("coremedia").exists
    assert host.user("content-management-server").shell == "/sbin/nologin"
    assert host.user("content-management-server").home == "/opt/coremedia/content-management-server"


def test_property_file(File):
    property_file = "/opt/coremedia/content-management-server/application.properties"
    content = File(property_file).content
    assert "cap.server.search.enable(.*)=(.*)false" in content


def test_properties(host):
    property_file = "/opt/coremedia/content-management-server/application.properties"
    content = host.file(property_file).content_string

    print(content)

    assert re.compile("cap.server.search.enable.*false").search(content)
    assert re.compile("cap.server.license.*=.*cms.zip").search(content)


def test_service(host):
    service = host.service("content-management-server")
    assert service.is_enabled
    assert service.is_running


@pytest.mark.parametrize("ports", [
    '0.0.0.0:40299',
    '0.0.0.0:40280',
    '0.0.0.0:40281'
])
def test_open_port(host, ports):

    for i in host.socket.get_listening_sockets():
        print( i )

    application = host.socket("tcp://{}".format(ports))
    assert application.is_listening
