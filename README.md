# CoreMedia - generic ansible role for content-server

This role can deploy all (currently known) content-server types.

Like *content-management-server*, *master-live-server* and *replication-live-server*.

Since CMCC-10 spring-boot is the preferred way to roll out an application (after docker).

Here, we use the spring-boot way.

With spring-boot a few special features were integrated:

- In the defaults.yml an extra configuration parameter `spring_boot_overwrite` was integrated.
  <br>Thats overwrite the default parameters in `vars/main.yml`
- A systemd unit configuration is created for each service so that the unit file does not have to be adjusted.
- JMX monitoring can now be explicitly enabled or deactivated.


Furthermore, it is no longer necessary to extend the IOR path with the service name for *spring-boot*.

Therefore `http://mls.cm.local:40280/master-live-server/ior` becomes `http://mls.cm.local:40280/ior`

Besides **mysql**, **postgres** is now supported as database type.
Here you only have to specify the corresponding type at `database_driver`.

## Requirement

- java 11 (e.g. [corretto](https://github.com/bodsch/ansible-corretto-java))
- [solr](https://gitlab.com/coremedia-as-code/deployment/ansible-roles/solr) (for the *content-management-server*)
- [license-manager](https://gitlab.com/coremedia-as-code/deployment/ansible-roles/license-manager)
- [coremedia](https://gitlab.com/coremedia-as-code/deployment/ansible-roles/coremedia)

## Dependent Services

### content-management-server

- solr
```
application_properties:
  solr.url: http://127.0.0.1:40080/solr
```

- master-live-server
```
application_properties:
  publisher.target.ior_url: http://mls.cm.local:40280/ior
```

- database
```
content_server_database:
  host: backend_database.int
  schema: cm_management
  user: cm_management
  password: cm_management
```

### master-live-server

- database
```
content_server_database:
  host: frontend_database.int
  schema: cm_master
  user: cm_master
  password: cm_master
```

### replication-live-server

- database
```
content_server_database:
  host: frontend_database.int
  schema: cm_replication
  user: cm_replication
  password: cm_replication
```

- master-live-server
```
application_properties:
  replicator.publicationIorUrl: http://mls.cm.local:40280/ior
```

**ATTENTION**

For both *master-live-server* and *replication-live-server*, the configuration variable `solr.collection.content` must be set, otherwise the content-servers will not start!

This variable is already set in the `vars/main.yml`.


## Configuration

To set a own *heap* memory use `heap_memory` (default are `512`). **Please use only Megabyte data!**

The default Out-Of-Memory Handler for *OnOutOfMemoryError* are `kill -9 %p`.
You can also use the [oom-notifier](https://gitlab.com/coremedia-as-code/deployment/ansible-roles/coremedia/-/blob/master/templates/oom-notifier.sh.j2) script of the User *coremedia* `/opt/coremedia/bin/oom-notifier.sh`.
For this purpose, only the `oom_notifier` variable must be set accordingly.

With `jmx_monitoring_enabled` you can enable / disable the JMX Monitoring Interface.

For more detailed information please look into the `default/main.yml` or in the `vars/main.yml`!

With `elastic_url_bulk` you can specify an elasticsearch server to establish a central logging.
As an example: `elastic_url_bulk: 'http://elastic.cm.local:9200/_bulk`


## Examples

- content-management-server

```
- hosts: content-management-server
  vars:
    coremedia_license_artifacts:
      - { service: 'cms', file: 'cs-license.zip', destination: 'cms.zip' }
  roles:
    - role: postgresql
    - role: corretto
    - role: solr
    - role: content-server
      vars:
        - content_server: content-management-server
        - coremedia:
            content_management_server:
              artefact_name: content-server.jar
```

- master-live-server

```
- hosts: content-management-server
  vars:
    coremedia_license_artifacts:
      - { service: 'mls', file: 'mls-license.zip', destination: 'mls.zip' }
  roles:
    - role: postgresql
    - role: corretto
    - role: content-server
      vars:
        - content_server: master-live-server
        - oom_notifier: /opt/coremedia/bin/oom-notifier.sh
        - coremedia:
            master_live_server:
              artefact_name: content-server.jar
```


## Tests

```
$ tox -e py36-ansible29 -- molecule test --all
$ tox -e py36-ansible29 -- molecule test -s content-management-server
$ tox -e py36-ansible29 -- molecule test -s master-live-server
$ tox -e py36-ansible29 -- molecule test -s replication-live-server
```

## Service relationships


![setup](content-server.png)
