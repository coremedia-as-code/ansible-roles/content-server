---

- name: reset local variables
  set_fact:
    local_application_checksum: ""
    application_checksum: ""
    artefact_name: ""
    artefact_checksum: ""
    artefact_checksum_file: ""
    application_name: "{{ content_server | regex_replace('-', '_') }}"

- name: do facts module to get latest information
  setup:
    gather_subset:
      - '!all'
      - '!any'
      - facter

# ---------------------------------------------------------------------------------------------

- name: set local application data
  set_fact:
    _local_application_data: "{{ ansible_local[ application_name ] }}"
  when: ansible_local[ application_name ] is defined

- name: set local application checksum
  set_fact:
    local_application_checksum: "{{ _local_application_data.checksum }}"
  when: _local_application_data is defined and _local_application_data.checksum is defined

# ---------------------------------------------------------------------------------------------

- name: detect application deployment data
  set_fact:
    _application_data: "{{ coremedia[ application_name ] }}"
  when: coremedia is defined and coremedia | count != 0

- name: set archive url
  set_fact:
    artefact_url: "{{ _application_data.artefact_url }}"
  when: _application_data is defined and _application_data.artefact_url is defined

- name: set archive name
  set_fact:
    artefact_name: "{{ _application_data.artefact_name }}"
  when: _application_data is defined and _application_data.artefact_name is defined

- name: set archive checksum
  set_fact:
    artefact_checksum: "{{ _application_data.artefact_checksum }}"
  when: _application_data is defined and _application_data.artefact_checksum is defined

- name: set archive checksum file
  set_fact:
    artefact_checksum_file: "{{ _application_data.artefact_checksum_file }}"
  when: _application_data is defined and _application_data.artefact_checksum_file is defined

- name: set local temporary path
  set_fact:
    local_tmp_directory: "{{ local_tmp_directory }}/{{ role_name }}"

- name: set content_server_user
  set_fact:
    content_server_user: "{{ content_server }}"

- name: merge spring_boot configuration between defaults and custom
  set_fact:
    spring_boot: "{{ spring_boot_defaults | combine( sprint_boot_overwrite, recursive=True ) }}"

- name: merge content_server_database configuration between defaults and custom
  set_fact:
    content_server_database: "{{ {} |
      combine( application_database_settings, recursive = True ) |
      combine( content_server_database, recursive = True ) }}"

# merge multiple application_properties variables:
#   - application_properties_defaults from vars/main.yml
#   - application_properties_custom from vars/main.yml
#   - application_properties from defaults/main.yml or from playbook
#
- name: merge application_properties configuration between defaults and custom
  set_fact:
    application_properties: "{{ application_properties_defaults |
      combine( application_properties_custom , recursive=True ) |
      combine( application_properties, recursive=True ) }}"

- name: add logfile to application_properties
  set_fact:
    application_properties: "{{ application_properties | combine( application_properties_elk, recursive=True ) }}"
  when: elastic_url_bulk is defined and elastic_url_bulk | length != 0

# ---------------------------------------------------------------------------------------------

- name: assert missing archive name
  assert:
    that:
      - artefact_name | length != 0
    msg: "application archive name is missing"
    quiet: true

# - name: assert missing archive  checksum
#   assert:
#     that:
#       - artefact_checksum | length != 0 or artefact_checksum_file | length != 0
#     msg: "application archive name or checksum is missing"
#     quiet: true

- name: do facts module to get latest information
  setup:
    gather_subset:
      - '!all'
      - '!any'
      - facter

# ---------------------------------------------------------------------------------------------

- debug:
    var="{{ item }}"
  when: item is defined
  loop:
    - content_server_database
